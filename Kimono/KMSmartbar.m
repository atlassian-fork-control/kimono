//
//  KMSmartbar.m
//  Kimono
//
//  Created by James Dumay on 29/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import "KMSmartbar.h"

@implementation KMSmartbar

@synthesize currentProgress = _currentProgress;

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self setDrawsBackground:NO];
    }
    return self;
}

- (void)update:(id<KMBrowserTab>)page
{
    if (page != nil)
    {
        double percentComplete = [page progress];
        if ([page loading])
        {
            _currentProgress = percentComplete;
        }
        else
        {
            _currentProgress = 0;
        }
        
        NSString *location = [page location] != nil ? [[page location] absoluteString] : @"";
        [self setStringValue:location];
    }
    else
    {
        _currentProgress = 0;
        [self setStringValue:@""];
    }
}

- (void)drawRect:(NSRect)dirtyRect
{

	
	
	// black outline
	NSRect blackOutlineFrame = NSMakeRect(0.0, 0.0, [self bounds].size.width, [self bounds].size.height-1.0);
	NSGradient *gradient = nil;
	if ([NSApp isActive]) {
		gradient = [[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:0.24 alpha:1.0] endingColor:[NSColor colorWithCalibratedWhite:0.374 alpha:1.0]];
	}
	else {
		gradient = [[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:0.55 alpha:1.0] endingColor:[NSColor colorWithCalibratedWhite:0.558 alpha:1.0]];
	}
	[gradient drawInBezierPath:[NSBezierPath bezierPathWithRoundedRect:blackOutlineFrame xRadius:3.6 yRadius:3.6] angle:90];
	
	
	// top inner shadow
	NSRect shadowFrame = NSMakeRect(1, 1, [self bounds].size.width-2.0, 10.0);
	[[NSColor colorWithCalibratedWhite:0.88 alpha:0.6] set];
	[[NSBezierPath bezierPathWithRoundedRect:shadowFrame xRadius:2.9 yRadius:2.9] fill];
	
	
	// main white area
	NSRect whiteFrame = NSMakeRect(1, 1, [self bounds].size.width-2.0, [self bounds].size.height-3.0);
    
    [[NSColor whiteColor] set];
    [[NSBezierPath bezierPathWithRoundedRect:whiteFrame xRadius:2.6 yRadius:2.6] fill];
    
    if (_currentProgress == 0)
    {
        [[NSColor whiteColor] set];
        [[NSBezierPath bezierPathWithRoundedRect:whiteFrame xRadius:2.6 yRadius:2.6] fill];
    }
    else
    {
        CGFloat progress = (CGFloat)_currentProgress;
        
        NSRect progressRect = whiteFrame;
        
        CGFloat progressWidth = progressRect.size.width * progress / 100;        
        progressRect.size.width = progressWidth;
        
        NSColor *startColor = [NSColor colorWithCalibratedRed:0.49 green:0.69 blue:0.86 alpha:1.00];
        NSColor *endColor = [NSColor colorWithCalibratedRed:0.57 green:0.81 blue:1.00 alpha:1.00];
        
        NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:progressRect xRadius:2.6 yRadius:2.6];
        [[[NSGradient alloc] initWithStartingColor:startColor endingColor:endColor] drawInBezierPath:path angle:90];
        
    }
    
	// draw the keyboard focus ring if we're the first responder and the application is active
	if (([[self window] firstResponder] == [self currentEditor]) && [NSApp isActive])
	{
		[NSGraphicsContext saveGraphicsState];
		NSSetFocusRingStyle(NSFocusRingOnly);
		[[NSBezierPath bezierPathWithRoundedRect:blackOutlineFrame xRadius:3.6 yRadius:3.6] fill];
		[NSGraphicsContext restoreGraphicsState];
	}
	else
	{
		// I don't like that the point to draw at is hard-coded, but it works for now
		[[self attributedStringValue] drawInRect:NSMakeRect(4.0, 3.0, [self bounds].size.width-8.0, [self bounds].size.width-6.0)];
	}
}

@end
