//
//  KMBrowserErrorViewController.m
//  Kimono
//
//  Created by James Dumay on 7/04/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import "KMBrowserErrorViewController.h"

@implementation KMBrowserErrorViewController
@synthesize errorLabel = _errorLabel;

- (NSString *)nibName
{
    return @"KMBrowserErrorViewController";
}

- (void)urlCouldNotBeLoaded:(NSURL*)url withMessage:(NSString*)message
{
    [_errorLabel setStringValue:message];
}

@end
