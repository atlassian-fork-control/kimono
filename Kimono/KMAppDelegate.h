//
//  KMAppDelegate.h
//  Kimono
//
//  Created by James Dumay on 23/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KMBrowserWindowController.h"

@interface KMAppDelegate : NSObject <NSApplicationDelegate>

@property (retain, nonatomic) NSMutableArray *controllers;

- (NSWindowController*)createNewWindow:(NSURL*)url;

- (IBAction)openNewWindow:(id)sender;
- (IBAction)openNewTab:(id)sender;
- (IBAction)focusSmartbar:(id)sender;
- (IBAction)stopLoad:(id)sender;
- (IBAction)reload:(id)sender;
- (IBAction)showTabs:(id)sender;
- (IBAction)closeTab:(id)sender;

- (KMBrowserWindowController *)currentBrowserWindow;

@end
