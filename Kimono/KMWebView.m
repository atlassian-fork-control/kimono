//
//  KMWebView.m
//  Kimono
//
//  Created by James Dumay on 12/04/12.
//  Copyright (c) 2012 Whimsy. All rights reserved.
//

#import "KMWebView.h"

@implementation KMWebView

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
        [self setApplicationNameForUserAgent:[NSString stringWithFormat:@"Kimono/%@-b%@", version, build]];
    }
    return self;
}

@end
