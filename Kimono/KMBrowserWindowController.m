//
//  NSBrowserWindowController.m
//  Kimono
//
//  Created by James Dumay on 28/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "KMBrowserWindowController.h"

@implementation KMBrowserWindowController
@synthesize toolbar = _toolbar;
@synthesize smartBar = _smartBar;
@synthesize smartBarToolbarItem = _smartBarToolbarItem;
@synthesize innerView = _innerView;
@synthesize tabsPopover = _tabsPopover;
@synthesize tabSearchField = _tabSearchField;
@synthesize tabsController = _tabsController;
@synthesize showTabListButton = _showTabListButton;
@synthesize tabTableView = _tabTableView;
@synthesize view = _view;
@synthesize tabControllers = _tabControllers;
@synthesize currentTab = _currentTab;

- (id)init {
    self = [super init];
    if (self) {
        _tabControllers = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSString *)windowNibName
{
    return @"KMBrowserWindow";
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    [_toolbar removeFromSuperview];
    [_tabTableView setBackgroundColor:[NSColor clearColor]];
}

- (void)showTabs
{
    [_toolbar setHidden:NO];
    [_tabsPopover showRelativeToRect:[_showTabListButton bounds] ofView:_showTabListButton preferredEdge:(CGRectMaxYEdge)];  
}

- (void)closeCurrentTab
{
    [[_currentTab view] removeFromSuperview];
    [_tabsController removeObject:_currentTab];
    _currentTab = nil;
}

- (void)closePopovers
{
    [_tabsPopover close];
}

- (IBAction)showActiveTabs:(NSSegmentedControl *)sender 
{
    [self showTabs];
}

- (IBAction)goBackOrForward:(NSSegmentedControl *)sender
{
    if ([sender selectedSegment] == 0)
    {
        [[_currentTab webView] goBack];
    }
    else
    {
        [[_currentTab webView] goForward];
    }
}

- (IBAction)smartBarActivated:(NSTextField *)sender 
{
    [[self window] makeFirstResponder:nil];
    
    NSString *urlAsString = [[sender stringValue] lowercaseString];
    
    if (![urlAsString hasPrefix:@"http://"] && ![urlAsString hasPrefix:@"https://"])
    {
        urlAsString = [@"http://" stringByAppendingString:urlAsString];
    }
    
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    
    if (_currentTab == nil)
    {
        [self createNewTab:url];
    }
    else
    {
        [_currentTab loadURL:url];
    }
}

- (void)setCurrentTab:(KMBrowserTabController *)tabController
{
    NSInteger indexOfSelectedTab = [_tabControllers indexOfObject:tabController];
    NSInteger indexOfCurrentTab = [_tabControllers indexOfObject:_currentTab];
    
    if ([_tabControllers count] > 1)
    {
        CATransition *trans = [CATransition animation];
        [trans setType:kCATransitionPush];
        [trans setSubtype:(indexOfCurrentTab < indexOfSelectedTab) ? kCATransitionFromTop : kCATransitionFromBottom];
        [trans setDuration:0.3];
    
        [[self innerView] setAnimations:[NSDictionary dictionaryWithObject:trans forKey:@"subviews"]];
    }
    
    [[tabController view] setFrame:[[self innerView] frame]];
    
    if (_currentTab == nil)
    {
        [[[self innerView] animator] addSubview:[tabController view]];
    }
    else
    {
        [[[self innerView] animator] replaceSubview:[_currentTab view] with:[tabController view]];
    }
    _currentTab = tabController;
    
    [self update:_currentTab];
}

- (KMBrowserTabController *)createNewTab:(NSURL *)url
{
    KMBrowserTabController *tabController = [[KMBrowserTabController alloc] initWithPageUpdater:self];
    [_tabsController addObject:tabController];
    
    [self setCurrentTab:tabController];
    
    [tabController loadURL:url];
    
    return tabController;
}

- (void)focusSmartbar
{
    //[_toolbar show:[_currentTab view]];
    [_smartBar selectText:self];
    [self closePopovers];
}

//TODO: release controller from the window array
- (void)windowWillClose
{
    NSLog(@"windowWillClose");
}

- (IBAction)searchTabs:(id)sender 
{
    NSSearchField *searchField = (NSSearchField*)sender;
    NSString *searchString = [searchField stringValue];
    NSPredicate *filter;
    if ([searchString length] == 0) 
    {
        filter = nil;
    }
    else
    {
        filter = [NSPredicate predicateWithFormat: @"pageTitle contains[cd] %@", searchString];
    }
    [_tabsController setFilterPredicate: filter];    
}

- (IBAction)closeTab:(id)sender
{
    NSButton *button = (NSButton*)sender;
    KMBrowserTabController *controller = [(NSTableCellView *)[button superview] objectValue];
    
    if (controller == _currentTab)
    {
        [[_currentTab view] removeFromSuperview];
        _currentTab = nil;
    }
    
    [_tabsController removeObject:controller];
    
    if ([_tabControllers count] == 0)
    {
        [self update:nil];
        [_tabsPopover close];
    }
}

- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
    NSInteger index = [_tabTableView selectedRow];
    if (index > -1 && index != [_tabControllers indexOfObject:_currentTab])
    {
        KMBrowserTabController *controller = [_tabControllers objectAtIndex:index];
        if (controller != nil)
        {
            [self setCurrentTab:controller];
        }
    }
}

- (void)reload
{
    [_currentTab reload];
}

- (void)stopLoad
{
    [_currentTab stopLoad];
}

- (void)update:(id<KMBrowserTab>)page
{
    if (page == _currentTab)
    {
        if (page == nil)
        {
            [[self window] setTitle:@""];
        }
        else
        {
            [[self window] setTitle:[page title]];
        }
        [_smartBar update:page];
        [_tabTableView reloadData];
    }
}

@end
